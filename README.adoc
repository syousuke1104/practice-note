= PRACTICE NOTE

このリポジトリは、 https://note.sysk-work.net/[^] のソースコードおよびコンテンツです。
サイトは、 https://www.11ty.dev[11ty (Eleventy)^] を使って生成し、 https://pages.cloudflare.com/[Cloudflare Pages^]でホストされています。

== 環境
`devcontainer.json` ファイルから作成できます。

== インストール

    npm ci

== 実行

Eleventyプロジェクトのため、以下のコマンドでビルドおよび確認することができます。

    npx eleventy --serve
