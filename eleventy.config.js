const eleventyAsciidoc = require("eleventy-plugin-asciidoc");
const { execSync } = require("child_process");

module.exports = function (eleventyConfig) {
    eleventyConfig.addPlugin(eleventyAsciidoc, {
        attributes: [
            "toc@",
            "toc-title=目次",
            "prewrap!",
            "source-highlighter=highlight.js"
        ],
        safe: "unsafe"
    });
    eleventyConfig.addGlobalData("siteTitle", "PRACTICE NOTE");

    eleventyConfig.addPassthroughCopy("src/css/hjs.min.css");
    eleventyConfig.addPassthroughCopy("src/js");
    eleventyConfig.addPassthroughCopy("src/**/*.png");
    eleventyConfig.addPassthroughCopy("src/**/*.svg");
    eleventyConfig.addPassthroughCopy("src/_redirects");

    eleventyConfig.ignores.add("src/**/examples/");

    eleventyConfig.on("eleventy.before", () => execSync("npx postcss ./src/css/article -d ./_site/css/article --ext min.css"))
    eleventyConfig.on("eleventy.after", () => execSync("./src/add-sitemap.sh"));

    eleventyConfig.setServerOptions({
        domDiff: false
    });

    eleventyConfig.addFilter("getCustomTagName", function (tag) {
        const tagsData = {
            dotnet: ".NET",
            csharp: "C#",
            network: "ネットワーク",
            gitlab: "GitLab",
            asciidoc: "AsciiDoc",
            openwrt: "OpenWrt",
            "11ty": "11ty (Eleventy)",
            linux: "Linux"
        };
        return tagsData[tag] || tag;
    });

    return {
        dir: {
            input: "src",
            output: "_site",
        },
    }
};
