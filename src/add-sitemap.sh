#!/bin/bash
find _site -path '*.html' |
grep -v '404.html' |
sed -e 's/^_site//' -e 's/index\.html$//' |
xargs -i echo "https://note.sysk-work.net{}" > _site/sitemap.txt
