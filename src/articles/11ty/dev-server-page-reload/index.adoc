---
date: 2023-08-20
---
= Eleventy ページ全体をリロードする

.環境
- Eleventy v2.0.1

Eleventyをインストール後、CLIで `npx eleventy --serve` を実行すると、開発用のWebサーバである link:https://github.com/11ty/eleventy-dev-server[Eleventy Dev Server^] が起動します。この状態でテンプレートなどのファイルを編集すると、HTML変換後にDOM差分を現在のページに適用してくれます。

私の環境では、ライブリロードでスタイルが崩れることがあったため、ページ全体を再読込する方法に変更しました。

== Eleventy Dev Server 設定変更
*.eleventy.js* ファイルで Dev Server の設定を変更します。 `setServerOptions` メソッドで `domDiff: false` を設定することで、差分更新の代わりにページを再読込します。

.ファイル名 *.eleventy.js*
[source,js]
----
module.exports = function(eleventyConfig) {
  eleventyConfig.setServerOptions({
    // DOM差分更新の代わりにページを再読込する
    domDiff: false
  });
};
----

他にもポート番号の変更や監視するファイルの追加などができるようです。詳細はマニュアルを参照してください。
https://www.11ty.dev/docs/dev-server/[Eleventy Dev Server — Eleventy]
