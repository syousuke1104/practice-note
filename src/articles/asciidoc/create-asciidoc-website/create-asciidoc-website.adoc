---
date: 2023-01-14
tags:
  - gitlab
---
= AsciiDocでWebサイトを作成する

CAUTION: 2023-07-09 現在、link:/articles/11ty/with-asciidoc[11ty (Eleventy) を使ったWebサイト構築]に移行しています。

AsciiDocで手軽にWebサイトをしたいが、AsciiDocをメインに完全サポートしているツールは https://antora.org/[Antora^] くらいしかない。

https://gitlab.eclipse.org/eclipse-wg/asciidoc-wg/asciidoc.org/-/blob/main/awesome-asciidoc.adoc#publish[awesome-asciidoc.adoc · main · Eclipse Working Groups / AsciiDoc WG / asciidoc.org · GitLab^]

静的サイト生成ツールを自作するのも面倒なので、 https://asciidoctor.org/[Asciidoctor^] の出力するHTMLをそのまま公開することにする。 +
ナビゲーション機能はまだ作ってないので、とりあえずヘッダーにトップページへのリンクだけでも貼っておく。

ついでにGitLab PagesでWebサイトの構築を自動化する。

== 概要
GitLab PagesとAsciidoctorを利用して、Webサイトを作成する方法を紹介する。

=== 仕様
* GitLab Pagesの機能で *public* ディレクトリを公開する。
* コンテンツは *public* ディレクトリ下で管理する。
* 公開するHTMLファイルは、 CI実行時に `asciidoctor` コマンドで *.adoc* ファイルから変換する。

.ディレクトリ構造例
[source]
----
📒 repository
  📂 public <.>
    📁 highlight <.>
    📂 modules <.>
      📂 asciidoc <.>
        📂 pages
          📄 example.adoc <.>
    📁 styles <.>
    📄 index.adoc <.>
  📄 .gitlab-ci.yml <.>
----
<.> 公開するWebコンテンツのルート。
<.> https://highlightjs.org/[Highlight.js^] コンテンツ。
<.> モジュールディレクトリ。モジュール: 管理するコンテンツのグループの単位。 +
*modules* 以下の構造はAntoraを参考にしている。 +
https://docs.antora.org/antora/latest/standard-directories/[^]
<.> モジュール名。何でも良い。
<.> 公開するページ。AsciidoctorでHTMLに変換し、*.html* ファイルとして公開する。
<.> CSSファイルを格納。
<.> トップページ。各ページへのリンクを集約。
<.> GitLab CI/CD設定ファイル。

== Webサイト作成

=== GitLab CI/CD設定
. GitLab上で空のプロジェクトを作成します。
. ローカルで下図のように *index.adoc* と *.gitlab-ci.yml* を作成します。
+
[source]
----
📒 repository
  📂 public
    📄 index.adoc
  📄 .gitlab-ci.yml
----
+
.ファイル名 *index.adoc*
[source,adoc]
----
include::examples/first-asciidoc.adoc[]
----
+
.ファイル名 *.gitlab-ci.yml*
[source,yaml]
----
image: asciidoctor/docker-asciidoctor <.>

pages:
  stage: deploy
  script:
    - asciidoctor public/index.adoc <.>
  artifacts:
    paths:
      - public <.>
    exclude:
      - "**/*.adoc" <.>
----
<.> 公式から https://hub.docker.com/r/asciidoctor/docker-asciidoctor/[Dockerイメージ^] が提供されているので、それを利用します。
<.> *index.adoc* ファイルをHTMLに変換し、同じディレクトリに出力します。
<.> ジョブの成果物を指定します。GitLab Pagesでは、公開するディレクトリを *public* にする必要があります。
<.> 変換元の *.adoc* ファイルは不要なので、成果物から除外します。
+
. ファイルをコミットおよびプッシュします。
+
作成したファイルをGitLab上にpushすると、自動的にパイプラインが実行されます。 +
パイプラインの実行状況は、サイドバーの *CI/CD* > *Pipelines* から確認できます。
+
image::https://imgur.com/eN99CP0.png[]
+
正常に実行されると **Settings > Pages** からWebページを確認できます。
+
image::https://imgur.com/ZTeXRDz.png[]
+
image::https://imgur.com/eYKzZI3.png[]

==== 追加のページを作成する
. *public* ディレクトリの下に *modules/<module>/pages/* ディレクトリを作成し、 *newpage.adoc* ファイルを追加します。
+
[source]
----
📒 repository
  📂 public
    📂 modules
      📂 asciidoc
        📂 pages
          📄 newpage.adoc <.>
    📄 index.adoc
  📄 .gitlab-ci.yml
----
+
. `asciidoctor` の変換対象ファイルに **public/{asterisk}{asterisk}/pages/{asterisk}.adoc** を追加します。
+
.ファイル名 *.gitlab-ci.yml*
[source,yaml]
----
...
  script:
    - asciidoctor public/index.adoc public/**/pages/*.adoc
...
----
+
これにより、 コンテンツをモジュール単位で分割して管理し、公開できます。

=== カスタムスタイルシート適用
CSSの適用は、AsciiDocの `stylesheet` 属性で設定します。 +
属性はファイルに書かず、コマンド引数で全体に適用します。

https://docs.asciidoctor.org/asciidoctor/latest/html-backend/custom-stylesheet/[Apply a Custom Stylesheet | Asciidoctor Docs^]

適用するCSSファイルは、 https://github.com/asciidoctor/asciidoctor-stylesheet-factory[Asciidoctor stylesheet factory^] で作成するか、 https://github.com/darshandsoni/asciidoctor-skins[asciidoctor-skins^] で公開されているものが利用できます。

. *public/styles/* にCSSファイルをコピーします。
+
[source]
----
📒 repository
  📂 public
    📁 modules
    📂 styles
      📄 site.css <.>
    📄 index.adoc
  📄 .gitlab-ci.yml
----
+
. 以下のように、 `linkcss` と `stylesheet` 属性をコマンド引数で設定します。
+
.ファイル名 *.gitlab-ci.yml*
[source,yaml]
----
...
  script:
    - asciidoctor public/index.adoc public/**/*.adoc
      -a linkcss <.>
      -a stylesheet="$CI_PAGES_URL/styles/site.css" <.>
...
----
<.> HTMLにCSSを埋め込む代わりにリンクさせます。
<.> HTTPでアクセスできるCSSファイルのパスを指定します。
+
[NOTE]
--
`$CI_PAGES_URL` は https://docs.gitlab.com/ee/ci/variables/predefined_variables.html[GitLab CI/CD変数^] です。 +
GitLab Pagesで公開するときのURLが設定されます。
--
+
[TIP]
--
正しく設定したのにスタイルシートが更新されない場合::
ブラウザやCDNのキャッシュが残っている可能性があります。
--

=== ソースハイライト設定
ソースブロックの構文ハイライトは、 `source-highlighter` 属性で設定します。 +
組み込みの構文ハイライターは以下の通りです。

* https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/highlightjs/[Highlight.js^]
* https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/rouge/[Rouge^]
* https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/pygments/[Pygments^]
* https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/coderay/[CodeRay^]

https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/[Syntax Highlighting | Asciidoctor Docs]

今回は、追加のライブラリをインストールする必要がない https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/highlightjs/[Highlight.js] を使います。

==== Highlight.js設定
. 以下のページからハイライトする言語を選んでダウンロードします。 +
https://highlightjs.org/download/[^]

. zipファイルを展開したら、 *highlight.min.js* ファイルと適用したい *.css* ファイル選んでを *public* 下にコピーします。
+
WARNING: *.css* ファイルは *github.min.css* に名前を変更する必要があります。
+
.追加ファイル
* `public/highlight/styles/github.min.css`
* `public/highlight/highlight.min.js`
+
[source]
----
📒 repository
  📂 public
    📂 highlight
      📂 styles
        📄 github.min.css <.>
      📄 highlight.min.js <.>
    📁 modules
    📁 styles
    📄 index.adoc
  📄 .gitlab-ci.yml
----
+
. *.gitlab-ci.yml* で、 `asciidoctor` コマンド引数にて `source-highlighter` と `highlightjsdir` 属性を設定します。
+
.ファイル名 *.gitlab-ci.yml*
[source,yaml]
----
...
  script:
    - asciidoctor public/index.adoc public/**/*.adoc
      -a linkcss
      -a stylesheet="$CI_PAGES_URL/styles/site.css"
      -a source-highlighter="highlight.js" <.>
      -a highlightjsdir="$CI_PAGES_URL/highlight" <.>
...
----

=== フッター作成
AsciiDocの https://docs.asciidoctor.org/asciidoc/latest/docinfo/[Docinfo^] 機能を使って、ヘッダーやフッターを作成できます。
link:../docinfo/[こちらでもDocinfoについて説明しています。]

今回は全ページに同じフッターを適用します。

. *public* と同じ階層に *docinfo* ディレクトリを作成します。
. *docinfo-footer.html* ファイルを作成します。フッターとして追加したい内容を書きます。
+
[source]
----
📒 repository
  📂 docinfo
    📄 docinfo-footer.html <.>
  📁 public
  📄 .gitlab-ci.yml
----
+
.ファイル名 *docinfo-footer.html*
[source,html]
----
<div id="footer">
    <div id="footer-text">
        <p class="tableblock text-center">
            Made with AsciiDoc.
        </p>
    </div>
</div>
----
+
.ファイル名 *.gitlab-ci.yml*
[source,yaml]
----
...
  script:
    - asciidoctor public/index.adoc public/**/*.adoc
      -a linkcss
      -a stylesheet="$CI_PAGES_URL/styles/site.css"
      -a source-highlighter="highlight.js"
      -a highlightjsdir="$CI_PAGES_URL/highlight"
      -a nofooter <.>
      -a docinfodir="$(pwd)/docinfo" <.>
      -a docinfo="shared" <.>
...
----
<.> 既存のフッターを完全に置き換える場合は `nofooter` 属性を設定します。
<.> Docinfoファイルが保存されているディレクトリを指定します。相対パスで指定した場合、*.adoc* ファイルからの相対パスになるため、絶対パスで指定しています。
<.> https://docs.asciidoctor.org/asciidoc/latest/docinfo/#enable[Docinfoを有効にする^] ための属性です。
