= AsciiDocでWebサイトを作成する

== AsciiDoc
以下、Wikipediaからの引用。
[quote,AsciiDoc - Wikipedia,https://ja.wikipedia.org/wiki/AsciiDoc]
____
link:https://ja.wikipedia.org/wiki/AsciiDoc[AsciiDoc]は軽量マークアップ言語のひとつである。
意味論的にはlink:https://ja.wikipedia.org/wiki/DocBook[DocBook] XMLと同一であるが、対人可読な文書記述形式であり、文書の(論理)構造を意味付ける規則が平文形式である。
ゆえに構文解析器を介することなく、テキストエディタなどを用いてAsciiDocで記述された文書を作成・閲読できる。
____
