---
date: 2023-11-03
---
= AsciiDocでHighlight.jsを利用する

AsciiDocでは、ソースハイライト (シンタックスハイライト) を有効化することで、ドキュメント内のソースコードを見やすくすることができます。

== Source Highlighting

AsciiDocがビルトインでサポートしているシンタックスハイライターは以下の通りです。

[cols="2,2,^,^,^,^"]
|===
|タイプ          |ライブラリ |HTML|PDF|EPUB3|Reveal.js

   |クライアントサイド|Highlight.js |✅|  |   |✅
.3+|ビルド時        |CodeRay       |✅|✅|✅|✅
                   |Pygments     ^|✅|✅|✅|✅
                   |Rouge        ^|✅|✅|✅|✅
|===

.参考リンク
- https://docs.asciidoctor.org/asciidoc/latest/verbatim/source-highlighter/[Source Highlighting | Asciidoctor Docs^]
- https://docs.asciidoctor.org/asciidoctor/latest/syntax-highlighting/[Syntax Highlighting | Asciidoctor Docs^]

上表の通り、link:https://highlightjs.org/[Highlight.js^]はクライアントサイドで動作するシンタックスハイライターです。JavaScriptで書かれており、ユーザのブラウザ上で実行されます。また、CDNから読み込めるため、ライブラリをインストールせずに利用可能です。

== Highlight.jsを有効化する

.環境
- Asciidoctor 2.0.12

[IMPORTANT]
以降の説明は、スタンドアロンのHTMLファイルを出力する場合のみ有効です。つまり、JekyllやEleventyなどのサイトジェネレータを利用した際の埋め込み用HTMLコード出力では機能しません。この場合は、公式ドキュメントの手順に従って実装してください。

AsciiDocでHighlight.jsを有効化するには、`source-highlighter` 属性をドキュメントヘッダーで設定する必要があります。

[source,asciidoc]
----
:source-highlighter: highlight.js
----

これにより、変換後のHTMLには、スタイルシートへのリンクとHighlight.jsを実行するコードが自動的に挿入されます。
また、出力されるソースブロックは、シンタックスハイライトが適用されるように、変換されます。

=== テーマを変更する
デフォルトのGitHubテーマから変更するには、`highlightjs-theme` 属性を設定します。

Highlight.js のテーマ一覧は、デモページで確認可能です。
https://highlightjs.org/demo[Demo - highlight.js^]

[source,adoc]
----
:highlightjs-theme: vs2015
:highlightjsdir: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0
----

また、`highlightjsdir` 属性では、ライブラリやスタイルシートを読み込むベースのURLを変更することが可能です。
デフォルトでリンクされているライブラリは、若干バージョンが古いことがあるため、明示的に設定する必要があるかもしれません。

.GitHubテーマ
image::https://imgur.com/LjHtpvm.png[]

.Visual Studio 2015テーマ
image::https://imgur.com/dPckX2F.png[]

.Dockerfile (Monokaiテーマ)
image::https://imgur.com/vCU7kbS.png[]

=== 対応言語を追加する
Highlight.jsのデフォルトでサポートされていない言語を追加するには、`highlightjs-languages` 属性を設定します。
複数ある場合は、`,` で区切ります。

[source,asciidoc]
----
:highlightjs-languages: dockerfile, powershell
----

== まとめ
- AsciiDocでは、クライアントサイドで動作するHighlight.jsを利用できます。
- Highlight.jsのテーマは変更可能です。
- ライブラリを読み込む場所を変更可能です。
- 対応言語を追加可能です。

.コード例
[source,asciidoc]
----
:source-highlighter: highlight.js
:highlightjs-theme: vs2015
:highlightjsdir: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0
:highlightjs-languages: dockerfile, powershell
----

[%autowidth,cols="m,"]
|===
|属性名|説明

|`source-highlighter`|利用するシンタックスハイライター
|`highlightjs-theme`|Highlight.jsテーマ
|`highlightjsdir`|Highlight.jsライブラリのパス
|`highlightjs-languages`|対応言語の追加
|===
