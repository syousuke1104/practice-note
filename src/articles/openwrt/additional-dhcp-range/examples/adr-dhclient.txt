root@dhcp-test:~# dhclient -r 
Killed old client process
root@dhcp-test:~# 
root@dhcp-test:~# 
root@dhcp-test:~# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: ens18: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 66:6f:7f:74:a2:1d brd ff:ff:ff:ff:ff:ff
    altname enp0s18
    inet6 fe80::646f:7fff:fe74:a21d/64 scope link 
       valid_lft forever preferred_lft forever
root@dhcp-test:~# 
root@dhcp-test:~# 
root@dhcp-test:~# dhclient
root@dhcp-test:~# 
root@dhcp-test:~# 
root@dhcp-test:~# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: ens18: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 66:6f:7f:74:a2:1d brd ff:ff:ff:ff:ff:ff
    altname enp0s18
    inet 10.1.130.155/24 brd 10.1.130.255 scope global dynamic ens18
       valid_lft 43197sec preferred_lft 43197sec
    inet6 fe80::646f:7fff:fe74:a21d/64 scope link 
       valid_lft forever preferred_lft forever
