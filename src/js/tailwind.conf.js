tailwind.config = {
    theme: {
        extend: {
            colors: {
                'primary': '#978dec',
            },
            fontFamily: {
                sans: ['Noto Sans JP', 'sans-serif']
            }
        }
    }
}
